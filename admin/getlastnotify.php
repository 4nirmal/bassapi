<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/dbclass.php';
include_once './admin.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$id = $_GET["id"];
if($id){

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$user = new Admin($connection);

$stmt = $user->getlastnotify($id);
$count = $stmt->rowCount();

if($count > 0){
    $products = array();
    $products["body"] = array();
    $products["count"] = $count;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $p  = array(
              "lastnotify" => $lastnotify
        );

        array_push($products["body"], $p);
    }

    echo json_encode($products);
}

else {

    echo json_encode();
}    
    
    
}
else{
    
}

?>