<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/dbclass.php';
include_once './admin.php';

$id = $_GET["id"];
$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$admin = new Admin($connection);

$stmt = $admin->get($id);
$count = $stmt->rowCount();

if($count > 0){


    $products = array();
    $products["body"] = array();
    $products["count"] = $count;

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);

        $p  = array(
              "id" => $id,
              "userid" => $userid,
              "name" => $name,
              "email" => $email,
              "password" => $password,
              "location" => $location,
              "entity" => $entity,
              "phone" => $phone,
        );

        array_push($products["body"], $p);
    }

    echo json_encode($products);
}

else {

echo json_encode();

}
?>