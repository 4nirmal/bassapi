<?php
class Admin{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "admin";

    // table columns
    public $id;
    public $userid;
    public $name;
    public $email;
    public $password;
    public $location;
    public $entity;
    public $phone;
    public $createdAt; 
    public $updatedAt;

    public function __construct($connection){
        $this->connection = $connection;
    }

    //C
    public function create(){
            // query to insert record
             $query = "INSERT INTO " . $this->table_name . " SET "
                     . "userid=:userid, name=:name, email=:email, password=:password, location=:location, entity=:entity, phone=:phone, createdAt=:createdAt";

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->userid=htmlspecialchars(strip_tags($this->userid));
             $this->name=htmlspecialchars(strip_tags($this->name));
             $this->email=htmlspecialchars(strip_tags($this->email));
             $this->password=htmlspecialchars(strip_tags($this->password));
             $this->location=htmlspecialchars(strip_tags($this->location));
             $this->entity=htmlspecialchars(strip_tags($this->entity));
             $this->phone=htmlspecialchars(strip_tags($this->phone));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
             $stmt->bindParam(":userid", $this->name);
             $stmt->bindParam(":name", $this->name);
             $stmt->bindParam(":email", $this->email);
             $stmt->bindParam(":password", $this->password);
             $stmt->bindParam(":location", $this->location);
             $stmt->bindParam(":entity", $this->entity);
             $stmt->bindParam(":phone", $this->phone);
             $stmt->bindParam(":createdAt", $this->createdAt);

             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;        
    }
    //R
    public function read(){
        $query = "SELECT p.id, p.userid, p.name, p.email, p.password, p.location, p.entity,p.phone  FROM " . $this->table_name . " p ";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function get($id){
        $query = "SELECT p.id, p.userid, p.name, p.email, p.password, p.location, p.entity,p.phone  FROM " . $this->table_name . " p where p.userid = '". $id ."'";
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }    

    public function login($id, $password){
        $query = "SELECT p.id, p.userid, p.name, p.email, p.password, p.location, p.entity,p.phone  FROM " . $this->table_name . " p where p.email = '". $id ."' and p.password ='". $password . "'";
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }  
    
    //U
    public function update($userid){
            // query to insert record
             $query = "UPDATE " . $this->table_name . " SET "
                     . "name=:name, email=:email, password=:password, location=:location, entity=:entity , updatedAt=:updatedAt, phone=:phone  WHERE userid = '". $userid. "'";

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->name=htmlspecialchars(strip_tags($this->name));
             $this->email=htmlspecialchars(strip_tags($this->email));
             $this->password=htmlspecialchars(strip_tags($this->password));
             $this->location=htmlspecialchars(strip_tags($this->location));
             $this->entity=htmlspecialchars(strip_tags($this->entity));
             $this->updatedAt=htmlspecialchars(strip_tags($this->updatedAt));
             $this->phone=htmlspecialchars(strip_tags($this->phone));

             // bind values

             $stmt->bindParam(":name", $this->name);
             $stmt->bindParam(":email", $this->email);
             $stmt->bindParam(":password", $this->password);
             $stmt->bindParam(":location", $this->location);
             $stmt->bindParam(":entity", $this->entity);
             $stmt->bindParam(":updatedAt", $this->updatedAt);
             $stmt->bindParam(":phone", $this->phone);

             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;        
    }
    
public function getlastnotify($id){
    $query = "SELECT p.lastnotify FROM " . $this->table_name . " p where p.userid = '". $id ."'";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

        return $stmt;
    }
public function updatelastnotify($userid){
            // query to insert record
   $query = "UPDATE " . $this->table_name . " SET "
   . "lastnotify=:lastnotify WHERE userid = '". $userid. "'";             

             // prepare query
   $stmt = $this->connection->prepare($query);
   
    $stmt->bindParam(":lastnotify", $this->lastnotify);
             // execute query
   if($stmt->execute()){
       return true;
   }

   return false;        
}
    //D
    public function delete(){}
}