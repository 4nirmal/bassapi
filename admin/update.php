<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './admin.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$admin = new Admin($connection);

$data = json_decode(file_get_contents("php://input"));

$admin->userid = $data->userid;
$admin->name = $data->name;
$admin->email = $data->email;
$admin->password = $data->password;
$admin->location = $data->location;
$admin->entity = $data->entity;
$admin->phone = $data->phone;
$admin->password = $data->password;
$admin->updatedAt = date('Y-m-d H:i:s');

if($admin->update($data->userid)){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
echo json_encode();
}
?>