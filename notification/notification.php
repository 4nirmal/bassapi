<?php
class Notification{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "notification";

    // table columns
    public $id;
    public $notification_id;
    public $type;
    public $receiver_id;
    public $notification_title;
    public $notification_message;
    public $createdAt;
    
    public function __construct($connection){
        $this->connection = $connection;
    }

    //C
    public function create(){
            // query to insert record
     $query = "INSERT INTO " . $this->table_name . " SET "
     . "notification_id=:notification_id, type=:type, receiver_id=:receiver_id, notification_title=:notification_title, notification_message=:notification_message, createdAt=:createdAt";

             // prepare query
     $stmt = $this->connection->prepare($query);

             // sanitize
     $this->notification_id=htmlspecialchars(strip_tags($this->notification_id));
     $this->type=htmlspecialchars(strip_tags($this->type));
     $this->receiver_id=htmlspecialchars(strip_tags($this->receiver_id));
     $this->notification_title=htmlspecialchars(strip_tags($this->notification_title));
     $this->notification_message=htmlspecialchars(strip_tags($this->notification_message));
     $this->createdAt=htmlspecialchars(strip_tags($this->createdAt));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
     $stmt->bindParam(":notification_id", $this->notification_id);
     $stmt->bindParam(":type", $this->type);
     $stmt->bindParam(":receiver_id", $this->receiver_id);
     $stmt->bindParam(":notification_title", $this->notification_title);
     $stmt->bindParam(":notification_message", $this->notification_message);
     $stmt->bindParam(":createdAt", $this->createdAt);

             // execute query
     if($stmt->execute()){
         return true;
     }

     return false;        
 }
    //R
 public function read(){
    $query = "SELECT p.id, p.notification_id, p.type, p.receiver_id, p.notification_title,p.notification_message,p.createdAt FROM " . $this->table_name . " p ";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

    return $stmt;
}
    
public function getbyuserid($receiver_id){
    $query = "SELECT p.id, p.notification_id, p.type, p.receiver_id, p.notification_title,p.notification_message,p.createdAt FROM " . $this->table_name . " p where p.receiver_id = '". $receiver_id . "'";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

    return $stmt;
}    
public function getlastnotify($notification_id){
    $query = "SELECT p.id, p.notification_id, p.type, p.receiver_id, p.notification_title,p.notification_message,p.createdAt FROM " . $this->table_name . " p where p.id > '". $notification_id . "'";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

    return $stmt;
}
    //U
    //C
public function update($notification_id){
            // query to insert record
 $query = "UPDATE " . $this->table_name . " SET "
 . "type=:type, receiver_id=:receiver_id, notification_title=:notification_title, notification_message=:notification_message, createdAt=:createdAt WHERE notification_id ='". $notification_id."'" ;

             // prepare query
 $stmt = $this->connection->prepare($query);

             // sanitize
 $this->notification_id=htmlspecialchars(strip_tags($this->notification_id));
 $this->type=htmlspecialchars(strip_tags($this->type));
 $this->receiver_id=htmlspecialchars(strip_tags($this->receiver_id));
 $this->notification_title=htmlspecialchars(strip_tags($this->notification_title));
 $this->notification_message=htmlspecialchars(strip_tags($this->notification_message));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
 $stmt->bindParam(":type", $this->type);
 $stmt->bindParam(":receiver_id", $this->receiver_id);
 $stmt->bindParam(":notification_title", $this->notification_title);
 $stmt->bindParam(":notification_message", $this->notification_message);
 $stmt->bindParam(":createdAt", $this->createdAt);

             // execute query
 if($stmt->execute()){
     return true;
 }

 return false;        
}
    //D
public function delete(){}
}