<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/dbclass.php';
include_once './notification.php';


$id = $_GET["id"];

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$notification = new Notification($connection);

$stmt = $notification->getlastnotify($id);
$count = $stmt->rowCount();

if($count > 0){


  $products = array();
  $products["body"] = array();
  $products["count"] = $count;

  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

    extract($row);

    $p  = array(
      "id" => $id,
      "notification_id"=>$notification_id,
      "type"=>$type,
      "receiver_id"=>$receiver_id,
      "notification_title"=>$notification_title,
      "notification_message"=>$notification_message,
      "createdAt"=>$createdAt
    );

    array_push($products["body"], $p);
  }

  echo json_encode($products);
}

else {

echo json_encode();
}
?>