<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './notification.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$notification = new Notification($connection);

$data = json_decode(file_get_contents("php://input"));

$notification->notification_id = $data->notification_id;
$notification->type = $data->type;
$notification->receiver_id = $data->receiver_id;
$notification->notification_title = $data->notification_title;
$notification->notification_message = $data->notification_message;
$notification->createdAt = date('Y-m-d H:i:s');

if($notification->update($data->notification_id)){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>