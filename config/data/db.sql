CREATE TABLE `admin` (
  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
  `userid` varchar(255),
  `name` varchar(255),
  `email` varchar(255),
  `password` varchar(255),
  `location` varchar(255),
  `entity` varchar(255),
  `createdAt` datetime NOT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `user` (
  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
  `userid` varchar(255),
  `name` varchar(255),
  `email` varchar(255),
  `password` varchar(255),
  `desc` varchar(255),
  `location` varchar(255),
  `entity` varchar(255),
  `createdAt` datetime NOT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `entity` (
  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
  `entityid` varchar(255),
  `name` varchar(255),
  `address` varchar(255),
  `location` varchar(255),
  `createdAt` datetime NOT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `leave` (
  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
  `leaveid` varchar(255),
  `entityid` varchar(255),
  `userid` varchar(255),
  `fromdate` varchar(255),
  `todate` varchar(255),
  `reason` varchar(255),
  `status` varchar(255),
  `createdAt` datetime NOT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `attendance` (
  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
  `datestr` varchar(255),
  `entityid` varchar(255),
  `userid` varchar(255),
  `intime` varchar(255),
  `outtime` varchar(255),
  `location` varchar(255),
  `status` varchar(255),
  `createdAt` datetime NOT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);