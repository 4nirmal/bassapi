<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/dbclass.php';
include_once './attendance.php';


$id = $_GET["id"];

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$leave = new Leave($connection);

$stmt = $leave->get($id);
$count = $stmt->rowCount();

if($count > 0){


    $products = array();
    $products["body"] = array();
    $products["count"] = $count;

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);

        $p  = array(
              "id" => $id,
              "datestr" => $datestr,
              "entityid" => $entityid,
              "userid" => $userid,
              "intime" => $intime,
              "outtime" => $outtime,
              "location" => $location,
            "outlocation" => $outlocation,
            "backuplocation" => $backuplocation,
              "status" => $status
        );

        array_push($products["body"], $p);
    }

    echo json_encode($products);
}

else {

echo json_encode();
}
?>