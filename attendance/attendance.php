<?php
class Attendance{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "attendance";

    // table columns
    public $id;
    public $datestr;
    public $entityid;
    public $userid;
    public $intime;
    public $outtime;
    public $location;
    public $backuplocation;
    public $outlocation;
    public $status;
    public $createdAt; 
    public $updatedAt;

    public function __construct($connection){
        $this->connection = $connection;
    }

    //C
    public function create(){
            // query to insert record
             $query = "INSERT INTO `" . $this->table_name . "` SET "
                     . "datestr=:datestr, entityid=:entityid, userid=:userid, intime=:intime, outtime=:outtime, location=:location, outlocation=:outlocation, backuplocation=:backuplocation, status=:status, createdAt=:createdAt";

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->datestr=htmlspecialchars(strip_tags($this->datestr));
             $this->entityid=htmlspecialchars(strip_tags($this->entityid));
             $this->userid=htmlspecialchars(strip_tags($this->userid));
             $this->intime=htmlspecialchars(strip_tags($this->intime));
             $this->outtime=htmlspecialchars(strip_tags($this->outtime));
             $this->location=htmlspecialchars(strip_tags($this->location));
             $this->backuplocation=htmlspecialchars(strip_tags($this->backuplocation));
             $this->outlocation=htmlspecialchars(strip_tags($this->outlocation));
             $this->status=htmlspecialchars(strip_tags($this->status));
            // $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
             $stmt->bindParam(":datestr", $this->datestr);
             $stmt->bindParam(":entityid", $this->entityid);
             $stmt->bindParam(":userid", $this->userid);
             $stmt->bindParam(":intime", $this->intime);
             $stmt->bindParam(":outtime", $this->outtime);
             $stmt->bindParam(":location", $this->location);
             $stmt->bindParam(":outlocation", $this->outlocation);
             $stmt->bindParam(":backuplocation", $this->backuplocation);
             $stmt->bindParam(":status", $this->status);
             $stmt->bindParam(":createdAt", $this->createdAt);
             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;        
    }
    //R
    public function read(){
        $query = "SELECT p.id, p.datestr, p.entityid, p.userid, p.intime, p.outtime, p.location, p.outlocation, p.backuplocation, p.status, p.createdat,p.updatedAt FROM `" . $this->table_name . "` p ";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }
 
    public function get($id){
        $query = "SELECT p.id, p.datestr, p.entityid, p.userid, p.intime, p.outtime, p.location, p.outlocation, p.backuplocation, p.status, p.createdat,p.updatedAt  FROM `" . $this->table_name . "` p where p.datestr = '". $id . "'";

        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        return $stmt;
    }    

    public function getbyuser($id){
        $query = "SELECT p.id, p.datestr, p.entityid, p.userid, p.intime, p.outtime, p.location, p.outlocation, p.backuplocation, p.status, p.createdat,p.updatedAt  FROM `" . $this->table_name . "` p where p.userid = '". $id . "'";

        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        return $stmt;
    }        

    public function getbydate($date){
        $query = "SELECT p.id, p.datestr, p.entityid, p.userid, p.intime, p.outtime, p.location, p.outlocation, p.backuplocation, p.status, p.createdat,p.updatedAt  FROM `" . $this->table_name . "` p where p.datestr = '". $date . "'";

        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        return $stmt;
    } 
    //U
    //C
    public function update($userid,$datestr){
            // query to insert record
             $query = "UPDATE " . $this->table_name . " SET "
                     . "outtime=:outtime, outlocation=:outlocation, backuplocation=:backuplocation, status=:status, updatedAt=:updatedAt WHERE userid = '". $userid. "' and datestr = '". $datestr. "'";

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->datestr=htmlspecialchars(strip_tags($this->datestr));
             $this->entityid=htmlspecialchars(strip_tags($this->entityid));
             $this->userid=htmlspecialchars(strip_tags($this->userid));
             $this->intime=htmlspecialchars(strip_tags($this->intime));
             $this->outtime=htmlspecialchars(strip_tags($this->outtime));
             $this->location=htmlspecialchars(strip_tags($this->location));
             $this->backuplocation=htmlspecialchars(strip_tags($this->backuplocation));
             $this->outlocation=htmlspecialchars(strip_tags($this->outlocation));
             $this->status=htmlspecialchars(strip_tags($this->status));
            // $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
             $stmt->bindParam(":outtime", $this->outtime);
             $stmt->bindParam(":outlocation", $this->outlocation);
             $stmt->bindParam(":backuplocation", $this->backuplocation);
             $stmt->bindParam(":status", $this->status);
             $stmt->bindParam(":updatedAt", $this->updatedAt);
             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;                
    }
    //D
    public function delete(){}
}