<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './attendance.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$attendance = new Attendance($connection);

$data = json_decode(file_get_contents("php://input"));

$attendance->datestr = $data->datestr;
$attendance->entityid = $data->entityid;
$attendance->userid = $data->userid;
$attendance->intime = $data->intime;
$attendance->outtime = $data->outtime;
$attendance->location = $data->location;
$attendance->outlocation = $data->outlocation;
$attendance->backuplocation = $data->backuplocation;
$attendance->status = $data->status;
$attendance->createdAt = date('Y-m-d H:i:s');

if($attendance->create()){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>