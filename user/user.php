<?php
class User{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "user";

    // table columns
    public $id;
    public $userid;
    public $name;
    public $email;
    public $description;
    public $password;
    public $location;
    public $entity;
    public $createdAt; 
    public $phone;
    public $gender; 
    public $updatedAt;
    public $mac_address;
    public $shift;

    public $lastnotify;
    public function __construct($connection){
        $this->connection = $connection;
    }

    //C
    public function create(){
            // query to insert record
       $query = "INSERT INTO " . $this->table_name . " SET "
       . "userid=:userid, name=:name, email=:email, description=:description, password=:password, location=:location, entity=:entity, phone=:phone, gender=:gender, mac_address=:mac_address, createdAt=:createdAt, shift=:shift";             

             // prepare query
       $stmt = $this->connection->prepare($query);

             // sanitize
       $this->userid=htmlspecialchars(strip_tags($this->userid));
       $this->name=htmlspecialchars(strip_tags($this->name));
       $this->email=htmlspecialchars(strip_tags($this->email));
       $this->description=htmlspecialchars(strip_tags($this->description));
       $this->password=htmlspecialchars(strip_tags($this->password));
       $this->location=htmlspecialchars(strip_tags($this->location));
       $this->entity=htmlspecialchars(strip_tags($this->entity));
       $this->phone=htmlspecialchars(strip_tags($this->phone));
       $this->gender=htmlspecialchars(strip_tags($this->gender));
       $this->mac_address=htmlspecialchars(strip_tags($this->mac_address));
       $this->shift=htmlspecialchars(strip_tags($this->shift));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
       $stmt->bindParam(":userid", $this->userid);
       $stmt->bindParam(":name", $this->name);
       $stmt->bindParam(":email", $this->email);
       $stmt->bindParam(":password", $this->password);
       $stmt->bindParam(":description", $this->description);
       $stmt->bindParam(":location", $this->location);
       $stmt->bindParam(":entity", $this->entity);
       $stmt->bindParam(":phone", $this->phone);
       $stmt->bindParam(":gender", $this->gender);
       $stmt->bindParam(":mac_address", $this->mac_address);
       $stmt->bindParam(":createdAt", $this->createdAt);
       $stmt->bindParam(":shift", $this->shift);
       
       

             // execute query
       if($stmt->execute()){
           return true;
       }

       return false;        
   }
    //R
   public function read(){
    $query = "SELECT p.id, p.userid, p.name, p.email, p.password,p.shift, p.description, p.location, p.entity, p.phone, p.gender, p.mac_address  FROM " . $this->table_name . " p ";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

    return $stmt;
}

public function get($id){
    $query = "SELECT p.id, p.userid, p.name, p.email, p.password, p.shift, p.description, p.location, p.entity, p.phone, p.gender, p.mac_address  FROM " . $this->table_name . " p where p.userid = '". $id ."'";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

        return $stmt;
    }    
    public function login($id, $password){
        $query = "SELECT p.id, p.userid, p.name, p.email, p.password, p.shift, p.description, p.location, p.entity, p.phone, p.gender, p.mac_address  FROM " . $this->table_name . " p where p.email = '". $id ."' and p.password='". $password ."'";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }  
    
    //U
public function update($userid){
            // query to insert record
   $query = "UPDATE " . $this->table_name . " SET "
   . "name=:name, email=:email, description=:description, password=:password, location=:location, entity=:entity, phone=:phone, gender=:gender, mac_address=:mac_address, shift=:shift, updatedAt=:updatedAt WHERE userid = '". $userid. "'";             

             // prepare query
   $stmt = $this->connection->prepare($query);

             // sanitize
   $this->name=htmlspecialchars(strip_tags($this->name));
   $this->email=htmlspecialchars(strip_tags($this->email));
   $this->description=htmlspecialchars(strip_tags($this->description));
   $this->password=htmlspecialchars(strip_tags($this->password));
   $this->location=htmlspecialchars(strip_tags($this->location));
   $this->entity=htmlspecialchars(strip_tags($this->entity));
   $this->phone=htmlspecialchars(strip_tags($this->phone));
   $this->gender=htmlspecialchars(strip_tags($this->gender));
   $this->mac_address=htmlspecialchars(strip_tags($this->mac_address));
   $this->shift=htmlspecialchars(strip_tags($this->shift));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
   
   $stmt->bindParam(":name", $this->name);
   $stmt->bindParam(":email", $this->email);
   $stmt->bindParam(":description", $this->description);
   $stmt->bindParam(":password", $this->password);
   $stmt->bindParam(":location", $this->location);
   $stmt->bindParam(":entity", $this->entity);
   $stmt->bindParam(":phone", $this->phone);
   $stmt->bindParam(":gender", $this->gender);
   $stmt->bindParam(":mac_address", $this->mac_address);
   $stmt->bindParam(":shift", $this->shift);
    $stmt->bindParam(":updatedAt", $this->updatedAt);
             // execute query
   if($stmt->execute()){
       return true;
   }

   return false;        
}

public function getlastnotify($id){
    $query = "SELECT p.lastnotify FROM " . $this->table_name . " p where p.userid = '". $id ."'";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

        return $stmt;
    }
public function updatelastnotify($userid){
            // query to insert record
   $query = "UPDATE " . $this->table_name . " SET "
   . "lastnotify=:lastnotify WHERE userid = '". $userid. "'";             

             // prepare query
   $stmt = $this->connection->prepare($query);
   
    $stmt->bindParam(":lastnotify", $this->lastnotify);
             // execute query
   if($stmt->execute()){
       return true;
   }

   return false;        
}
    //D
public function delete(){}
}