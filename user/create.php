<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './user.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$user = new User($connection);

$data = json_decode(file_get_contents("php://input"));

$user->userid = "user-". mt_rand(1000, 9999);
$user->name = $data->name;
$user->email = $data->email;
$user->password = $data->password;
$user->description = $data->description;
$user->location = $data->location;
$user->entity = $data->entity;
$user->phone = $data->phone;
$user->gender = $data->gender;
$user->mac_address = $data->mac_address;
$user->shift = $data->shift;
$user->createdAt = date('Y-m-d H:i:s');

if($user->create()){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>