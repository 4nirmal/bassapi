<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './user.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$user = new User($connection);

$data = json_decode(file_get_contents("php://input"));

$user->userid = $data->userid;
$user->lastnotify = (int)$data->lastnotify;
$user->updatedAt = date('Y-m-d H:i:s');

if($user->updatelastnotify($data->userid)){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>