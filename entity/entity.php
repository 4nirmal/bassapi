<?php
class Entity{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "entity";

    // table columns
    public $id;
    public $entityid;
    public $name;
    public $adress;
    public $location;
    public $createdAt; 
    public $updatedAt;

    public function __construct($connection){
        $this->connection = $connection;
    }

    //C
    public function create(){
            // query to insert record
             $query = "INSERT INTO " . $this->table_name . " SET "
                     . "entityid=:entityid, name=:name, address=:address, location=:location, createdAt=:createdAt";

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->entityid=htmlspecialchars(strip_tags($this->entityid));
             $this->name=htmlspecialchars(strip_tags($this->name));
             $this->address=htmlspecialchars(strip_tags($this->address));
             $this->location=htmlspecialchars(strip_tags($this->location));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
             $stmt->bindParam(":entityid", $this->entityid);
             $stmt->bindParam(":name", $this->name);
             $stmt->bindParam(":address", $this->address);
             $stmt->bindParam(":location", $this->location);
             $stmt->bindParam(":createdAt", $this->createdAt);

             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;        
    }
    //R
    public function read(){
        $query = "SELECT p.id, p.entityid, p.name, p.address, p.location FROM " . $this->table_name . " p ";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }
 
    public function get($id){
        $query = "SELECT p.id, p.entityid, p.name, p.address, p.location FROM " . $this->table_name . " p where p.entityid = '". $id . "'";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }    
    
    //U
    //C
    public function update($entityid){
            // query to insert record
             $query = "UPDATE " . $this->table_name . " SET "
                     . "name=:name, address=:address, location=:location, updatedAt=:updatedAt WHERE entityid ='". $entityid."'" ;

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->entityid=htmlspecialchars(strip_tags($this->entityid));
             $this->name=htmlspecialchars(strip_tags($this->name));
             $this->address=htmlspecialchars(strip_tags($this->address));
             $this->location=htmlspecialchars(strip_tags($this->location));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
             $stmt->bindParam(":name", $this->name);
             $stmt->bindParam(":address", $this->address);
             $stmt->bindParam(":location", $this->location);
             $stmt->bindParam(":updatedAt", $this->updatedAt);

             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;        
    }
    //D
    public function delete(){}
}