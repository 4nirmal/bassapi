<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './entity.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$entity = new Entity($connection);

$data = json_decode(file_get_contents("php://input"));

$entity->entityid = "entity-". mt_rand(100, 9999);
$entity->name = $data->name;
$entity->address = $data->address;
$entity->location = $data->location;
$entity->createdAt = date('Y-m-d H:i:s');

if($entity->create()){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>