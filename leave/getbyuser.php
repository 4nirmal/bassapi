<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/dbclass.php';
include_once './leave.php';


$id = $_GET["id"];

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$leave = new Leave($connection);

$stmt = $leave->getbyuser($id);
$count = $stmt->rowCount();

if($count > 0){


    $products = array();
    $products["body"] = array();
    $products["count"] = $count;

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);

        $p  = array(
              "id" => $id,
              "leaveid" => $leaveid,
              "entityid" => $entityid,
              "userid" => $userid,
              "from" => $fromdate,
              "to" => $todate,
              "reason" => $reason,
              "status" => $status,
              "username" => $username,
              "updatedby" => $updatedby,
              "comment" => $comment
        );

        array_push($products["body"], $p);
    }

    echo json_encode($products);
}

else {

echo json_encode();
}
?>