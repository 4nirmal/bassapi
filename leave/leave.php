<?php
class Leave{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "leave";

    // table columns
    public $id;
    public $leaveid;
    public $entityid;
    public $userid;
    public $from;
    public $username;
    public $to;
    public $reason;
    public $status;
    public $updatedby;
    public $createdAt; 
    public $comment;
    public $updatedAt;

    public function __construct($connection){
        $this->connection = $connection;
    }

    //C
    public function create(){
            // query to insert record
             $query = "INSERT INTO `" . $this->table_name . "` SET "
                     . "leaveid=:leaveid, entityid=:entityid, userid=:userid, username=:username, fromdate=:from, todate=:to, reason=:reason, status=:status,comment=:comment, updatedby=:updatedby,createdAt=:createdAt";

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->leaveid=htmlspecialchars(strip_tags($this->leaveid));
             $this->entityid=htmlspecialchars(strip_tags($this->entityid));
             $this->userid=htmlspecialchars(strip_tags($this->userid));
             $this->username=htmlspecialchars(strip_tags($this->username));
             $this->from=htmlspecialchars(strip_tags($this->from));
             $this->to=htmlspecialchars(strip_tags($this->to));
             $this->reason=htmlspecialchars(strip_tags($this->reason));
             $this->status=htmlspecialchars(strip_tags($this->status));
             $this->updatedby=htmlspecialchars(strip_tags($this->updatedby));
             $this->comment=htmlspecialchars(strip_tags($this->comment));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
             $stmt->bindParam(":leaveid", $this->leaveid);
             $stmt->bindParam(":entityid", $this->entityid);
             $stmt->bindParam(":userid", $this->userid);
             $stmt->bindParam(":from", $this->from);
             $stmt->bindParam(":to", $this->to);
             $stmt->bindParam(":reason", $this->reason);
             $stmt->bindParam(":status", $this->status);
             $stmt->bindParam(":createdAt", $this->createdAt);
             $stmt->bindParam(":username", $this->username);
             $stmt->bindParam(":updatedby", $this->updatedby);
             $stmt->bindParam(":comment", $this->comment);
             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;        
    }
    //R
    public function read(){
        $query = "SELECT p.id, p.leaveid, p.entityid, p.userid, p.fromdate, p.todate, p.reason, p.username, p.updatedby, p.comment, p.status FROM `" . $this->table_name . "` p ";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }
 
    public function get($id){
        $query = "SELECT p.id, p.leaveid, p.entityid, p.userid, p.fromdate, p.todate, p.reason, p.username, p.updatedby, p.comment, p.status FROM `" . $this->table_name . "` p where p.leaveid = '". $id. "'";

        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        return $stmt;
    }    
    public function getbyuser($id){
        $query = "SELECT p.id, p.leaveid, p.entityid, p.userid, p.fromdate, p.todate, p.reason, p.username, p.updatedby, p.comment, p.status FROM `" . $this->table_name . "` p where p.userid = '". $id . "'";

        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        return $stmt;
    }        
    
    //U
    //C
    public function update($leaveid){
            // query to insert record
             $query = "UPDATE `".$this->table_name."` SET "
                     . "status=:status,  updatedby=:updatedby,  comment=:comment, updatedAt=:updatedAt where leaveid='".$leaveid."'";

             // prepare query
             $stmt = $this->connection->prepare($query);

             // sanitize
             $this->leaveid=htmlspecialchars(strip_tags($this->leaveid));
             $this->entityid=htmlspecialchars(strip_tags($this->entityid));
             $this->userid=htmlspecialchars(strip_tags($this->userid));
             $this->username=htmlspecialchars(strip_tags($this->username));
             $this->from=htmlspecialchars(strip_tags($this->from));
             $this->to=htmlspecialchars(strip_tags($this->to));
             $this->reason=htmlspecialchars(strip_tags($this->reason));
             $this->status=htmlspecialchars(strip_tags($this->status));
             $this->updatedby=htmlspecialchars(strip_tags($this->updatedby));
             $this->comment=htmlspecialchars(strip_tags($this->comment));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
             $stmt->bindParam(":status", $this->status);
             $stmt->bindParam(":updatedAt", $this->updatedAt);
             $stmt->bindParam(":updatedby", $this->updatedby);
             $stmt->bindParam(":comment", $this->comment);

             // execute query
             if($stmt->execute()){
                 return true;
             }

             return false;        
    }
    //D
    public function delete(){}
}