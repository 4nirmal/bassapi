<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './leave.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$leave = new Leave($connection);

$data = json_decode(file_get_contents("php://input"));

$leave->leaveid = $data->leaveid;
$leave->status = $data->status;
$leave->updatedby = $data->updatedby;
$leave->comment = $data->comment;
$leave->updatedAt = date('Y-m-d H:i:s');

if($leave->update($data->leaveid)){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>