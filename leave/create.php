<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './leave.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$leave = new Leave($connection);

$data = json_decode(file_get_contents("php://input"));

$leave->leaveid = "leave-". mt_rand(100, 9999);
$leave->entityid = $data->entityid;
$leave->userid = $data->userid;
$leave->from = $data->from;
$leave->username = $data->username;
$leave->to = $data->to;
$leave->reason = $data->reason;
$leave->status = $data->status;
$leave->createdAt = date('Y-m-d H:i:s');
$leave->updatedby = $data->updatedby;
$leave->comment = $data->comment;

if($leave->create()){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>