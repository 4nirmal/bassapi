<?php
class Shift{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "shift";

    // table columns
    public $id;
    public $shift_from;
    public $shift_to;
    public $shift_name;

    public function __construct($connection){
        $this->connection = $connection;
    }

    //C
    public function create(){
            // query to insert record
     $query = "INSERT INTO " . $this->table_name . " SET "
     . "shift_from=:shift_from, shift_to=:shift_to, shift_name=:shift_name";

             // prepare query
     $stmt = $this->connection->prepare($query);

             // sanitize
     $this->shift_name=htmlspecialchars(strip_tags($this->shift_name));
     $this->shift_from=htmlspecialchars(strip_tags($this->shift_from));
     $this->shift_to=htmlspecialchars(strip_tags($this->shift_to));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
     $stmt->bindParam(":shift_name", $this->shift_name);
     $stmt->bindParam(":shift_from", $this->shift_from);
     $stmt->bindParam(":shift_to", $this->shift_to);

             // execute query
     if($stmt->execute()){
         return true;
     }

     return false;        
 }
    //R
 public function read(){
    $query = "SELECT p.id, p.shift_to, p.shift_from, p.shift_name FROM " . $this->table_name . " p ";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

    return $stmt;
}

public function get($id){
    $query = "SELECT p.id, p.shift_to, p.shift_from, p.shift_name FROM " . $this->table_name . " p where p.id = '". $id . "'";

    $stmt = $this->connection->prepare($query);

    $stmt->execute();

    return $stmt;
}    

    //U
    //C
public function update($id){
            // query to insert record
 $query = "UPDATE " . $this->table_name . " SET "
 . "shift_from=:shift_from, shift_to=:shift_to, shift_name=:shift_name WHERE id ='". $id."'" ;

             // prepare query
 $stmt = $this->connection->prepare($query);

             // sanitize
     $this->shift_name=htmlspecialchars(strip_tags($this->shift_name));
     $this->shift_from=htmlspecialchars(strip_tags($this->shift_from));
     $this->shift_to=htmlspecialchars(strip_tags($this->shift_to));
//             $this->createdAt=htmlspecialchars(strip_tags($this->createdat));

             // bind values
     $stmt->bindParam(":shift_name", $this->shift_name);
     $stmt->bindParam(":shift_from", $this->shift_from);
     $stmt->bindParam(":shift_to", $this->shift_to);

             // execute query
 if($stmt->execute()){
     return true;
 }

 return false;        
}
    //D
public function delete(){}
}