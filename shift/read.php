<?php
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/dbclass.php';
include_once './shift.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$shift = new Shift($connection);

$stmt = $shift->read();
$count = $stmt->rowCount();

if($count > 0){


  $products = array();
  $products["body"] = array();
  $products["count"] = $count;

  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

    extract($row);

 
    $p  = array(
      "id" => $id,
      "shift_from"=>$shift_from,
      "shift_to"=>$shift_to,
      "shift_name"=>$shift_name
    );


    array_push($products["body"], $p);
  }

  echo json_encode($products);
}

else {

echo json_encode();
}
?>