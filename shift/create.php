<?php

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/dbclass.php';
include_once './shift.php';

$dbclass = new DBClass();
$connection = $dbclass->getConnection();

$shift = new Shift($connection);

$data = json_decode(file_get_contents("php://input"));

$shift->shift_name = "shift-". mt_rand(100, 9999);
$shift->shift_to = $data->shift_to;
$shift->shift_from = $data->shift_from;

if($shift->create()){
$products = array();
    $products["message"] = "Success";
echo json_encode($products);
}
else{
  $products = array();
    $products["message"] = "Failed";
echo json_encode($products);
}
?>